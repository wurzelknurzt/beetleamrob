# !/usr/bin/env python

# File: main.cpp
# Author: Steffen
# Last Changes: 05.12.2018

import rospy
from std_msgs.msg import Int64

from enum import Enum
import time

# import methods with formatted laser data

# states
class STATES(Enum):
    CHECK_FRONT = 1
    CHECK_FRONT_LR = 2
    CHECK_BACK = 3
    CHECK_LR = 4
    DRIVE_FORWARD = 5
    DRIVE_BACKWARD = 6
    STEER_LEFT = 7
    STEER_RIGHT = 8
    ERROR = 9

# ''' ********** TEMP ********** TEMP ********** TEMP ********** '''
class SCANNER:
    def getVorne(self):
        return 100
    def getVorneRechts(self):
        return 100
    def getVorneLinks(self):
        return 100

    def getRechts(self):
        return 100
    def getLinks(self):
        return 100
    def getHinten(self):
        return 100
# ''' ********** TEMP ********** TEMP ********** TEMP ********** '''


# ''' ********** CLASS FSM ********** '''
class FSM:
    # initial state
    state = STATES.CHECK_FRONT
    nextState = STATES.CHECK_FRONT
    prevState = STATES.CHECK_FRONT

    # distance params
    distance_normal_front = 100
    distance_normal_front_left = 100
    distance_normal_front_right = 100
    distance_normal_left = 100
    distance_normal_right = 100
    distance_normal_back = 100
    distance_backward_back = 30

    # ''' ********** METHOD DRIVE STOPP ********** '''
    def driveStopp(self):
	i = 1
        rospy.init_node('Ride')
	publisher=rospy.Publisher('/Ride', Int64, queue_size=1)
	rate=rospy.Rate(20)
	time.sleep(.1)
	
	while not rospy.is_shutdown():
		publisher.publish(0)
		rate.sleep()
		if i>1:
			break
		i=i+1

    # ''' ********** METHOD DRIVE FORWARD ********** '''
    def driveForward(self):
	i = 1
        rospy.init_node('Ride')
	publisher=rospy.Publisher('/Ride', Int64, queue_size=1)
	rate=rospy.Rate(20)
	time.sleep(.1)
	
	while not rospy.is_shutdown():
		publisher.publish(1)
		rate.sleep()
		if i>1:
			break
		i=i+1

    # ''' ********** METHOD DRIVE BACKWARD ********** '''
    def driveBackward(self):
	i = 1
        rospy.init_node('Ride')
	publisher=rospy.Publisher('/Ride', Int64, queue_size=1)
	rate=rospy.Rate(20)
	time.sleep(.1)
	
	while not rospy.is_shutdown():
		publisher.publish(2)
		rate.sleep()
		if i>1:
			break
		i=i+1

 # ''' ********** METHOD STEER MIDDLE********** '''
    def steerMiddle(self):
        i = 1
        rospy.init_node('Ride')
	publisher=rospy.Publisher('/servoAngel', Int64, queue_size=1)
	rate=rospy.Rate(20)
	time.sleep(.1)
	
	while not rospy.is_shutdown():
		publisher.publish(0)
		rate.sleep()
		if i>1:cd
			break
		i=i+1

    # ''' ********** METHOD STEER RIGHT ********** '''
    def steerRight(self):
        i = 1
        rospy.init_node('Ride')
	publisher=rospy.Publisher('/servoAngel', Int64, queue_size=1)
	rate=rospy.Rate(20)
	time.sleep(.1)
	
	while not rospy.is_shutdown():
		publisher.publish(1)
		rate.sleep()
		if i>1:
			break
		i=i+1

    # ''' ********** METHOD STEER LEFT ********** '''
    def steerLeft(self):
        i = 1
        rospy.init_node('Ride')
	publisher=rospy.Publisher('/servoAngel', Int64, queue_size=1)
	rate=rospy.Rate(20)
	time.sleep(.1)
	
	while not rospy.is_shutdown():
		publisher.publish(2)
		rate.sleep()
		if i>1:
			break
		i=i+1

    # ''' ********** METHOD EVAL EVENTS ********** '''
    # where to go when in current state and something happens
    def evalEvents(self):

        self.prevState = self.state
        self.state = self.nextState

        # ''' ~~~~~ CHECK FRONT ~~~~~ '''
        if self.state == STATES.CHECK_FRONT:
            if SCANNER.getVorne(self) >= self.distance_normal_front:
                self.nextState = STATES.DRIVE_FORWARD
            else:
                self.nextState = STATES.CHECK_FRONT_LR

        # ''' ~~~~~ CHECK FRONT LR ~~~~~ '''
        if self.state == STATES.CHECK_FRONT_LR:
            if (SCANNER.getVorneLinks(self) >= self.distance_normal_front_left) and (SCANNER.getVorneRechts(self) < self.distance_normal_front_right):
                self.nextState = STATES.STEER_LEFT
            elif (SCANNER.getVorneRechts(self) >= self.distance_normal_front_right) and (SCANNER.getVorneLinks(self) < self.distance_normal_front_left):
                self.nextState = STATES.STEER_RIGHT
            elif (SCANNER.getVorneRechts(self) >= self.distance_normal_front_right) and (SCANNER.getVorneLinks(self) >= self.distance_normal_front_left):
                self.nextState = STATES.STEER_RIGHT
            else:
                self.nextState = STATES.CHECK_BACK

        # ''' ~~~~~ CHECK BACK ~~~~~ '''
        if self.state == STATES.CHECK_BACK:
            if SCANNER.getHinten(self) >= self.distance_normal_back:
                self.nextState = STATES.DRIVE_BACKWARD
            else:
                self.nextState = STATES.CHECK_LR

        # ''' ~~~~~ CHECK LR ~~~~~ '''
        if self.state == STATES.CHECK_LR:
            if (SCANNER.getLinks(self) >= self.distance_normal_left) and (SCANNER.getRechts(self) < self.distance_normal_right):
                self.nextState = STATES.STEER_RIGHT
            elif (SCANNER.getRechts(self) >= self.distance_normal_right) and (SCANNER.getLinks(self) < self.distance_normal_left):
                self.nextState = STATES.STEER_LEFT
            elif SCANNER.getHinten(self) >= self.distance_backward_back:
                self.nextState = STATES.DRIVE_BACKWARD

        # ''' ~~~~~ DRIVE FORWARD ~~~~~ '''
        if self.state == STATES.DRIVE_FORWARD:
            self.nextState = STATES.CHECK_FRONT

        # ''' ~~~~~ DRIVE BACKWARD ~~~~~ '''
        if self.state == STATES.DRIVE_BACKWARD:
            self.nextState = STATES.CHECK_BACK

        # ''' ~~~~~ STEER LEFT ~~~~~ '''
        if self.state == STATES.STEER_LEFT:

            if self.prevState == STATES.CHECK_FRONT_LR:
                self.nextState = STATES.DRIVE_FORWARD
            elif self.prevState == STATES.CHECK_LR:
                    self.nextState = STATES.DRIVE_BACKWARD

        # ''' ~~~~~ STEER RIGHT ~~~~~ '''
        if self.state == STATES.STEER_RIGHT:
            if self.prevState == STATES.CHECK_FRONT_LR:
                self.nextState = STATES.DRIVE_FORWARD
            elif self.prevState == STATES.CHECK_LR:
                self.nextState = STATES.DRIVE_BACKWARD

        if self.state == STATES.ERROR:
            self.nextState = STATES.CHECK_FRONT

    # ''' ********** METHOD EVAL STATE ********** '''
    def evalState(self):
        if self.state == STATES.CHECK_FRONT:
            print("CHECK_FRONT")  # -----> debugging
            self.driveStopp()
	    self.steerMiddle()

        if self.state == STATES.CHECK_FRONT_LR:
            print("CHECK_FRONT_LR")  # -----> debugging
           self.driveStopp()
	   self.steerMiddle()

        if self.state == STATES.CHECK_BACK:
            print("CHECK_BACK")  # -----> debugging
            self.driveStopp()
	    self.steerMiddle()

        if self.state == STATES.CHECK_LR:
            print("CHECK_LR")  # -----> debugging
            self.driveStopp()
	    self.steerMiddle()

        if self.state == STATES.DRIVE_FORWARD:
            print("DRIVE_FORWARD")  # -----> debugging
            self.driveForward()
	    self.steerMiddle()

        if self.state == STATES.DRIVE_BACKWARD:
            print("DRIVE_BACKWARD")  # -----> debugging
            self.driveBackward()
	    self.steerMiddle()

        if self.state == STATES.STEER_LEFT:
            print("STEER_LEFT")  # -----> debugging
	    self.driveStopp()
            self.steerLeft()

        if self.state == STATES.STEER_RIGHT:
            print("STEER_RIGHT")  # -----> debugging
	    self.driveStopp()
            self.steerRight()

        if self.state == STATES.ERROR:
            print("ERROR")  # -----> debugging
            # nothing to do here
