#! /usr/bin/env python

import rospy
from std_msgs.msg import Int64

def test():
	i=0
	rospy.init_node('Test')
	publisher=rospy.Publisher('/Ride', Int64, queue_size=1)
	rate=rospy.Rate(1)
	while not rospy.is_shutdown() and not i>1:
		publisher.publish(1)
		i=i+2
		rate.sleep()


			