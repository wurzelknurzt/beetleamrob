# !/usr/bin/env python

# File: main.cpp
# Author: Steffen
# Last Changes: 05.12.2018

import rospy
from std_msgs.msg import Int64
from sensor_msgs.msg import LaserScan

from enum import Enum
import time

# states
class STATES(Enum):
    CHECK_FRONT = 1
    CHECK_FRONT_LR = 2
    CHECK_BACK = 3
    CHECK_LR = 4
    DRIVE_FORWARD = 5
    DRIVE_BACKWARD = 6
    STEER_LEFT = 7
    STEER_RIGHT = 8
    ERROR = 9

# fsm
class FSM:
    # initial state
    state = STATES.CHECK_FRONT
    nextState = STATES.CHECK_FRONT
    prevState = STATES.CHECK_FRONT

    # distance params
    distance_normal_front = 100.0
    distance_normal_front_left = 100.0
    distance_normal_front_right = 100.0
    distance_normal_left = 100.0
    distance_normal_right = 100.0
    distance_normal_back = 100.0
    distance_backward_back = 30.0

    # initial values
    minimalVorne = 0.0
    minimalVorneLinks = 0.0
    minimalVorneRechts = 0.0
    minimalHinten = 0.0
    minimalLinks = 0.0
    minimalRechts = 0.0

    def spinner(self):
        rospy.init_node('scan_values')
        sub = rospy.Subscriber('/scan', LaserScan, self.callback)
        rospy.spin()

    def callback(self, msg):
        vorne = [msg.ranges[353], msg.ranges[354], msg.ranges[355], msg.ranges[356], msg.ranges[357], msg.ranges[358], msg.ranges[359], msg.ranges[0], msg.ranges[1], msg.ranges[2], msg.ranges[3], msg.ranges[4], msg.ranges[5], msg.ranges[6], msg.ranges[7]]
        self.minimalVorne = min(vorne)

        vorneLinks = [msg.ranges[7], msg.ranges[8], msg.ranges[9], msg.ranges[10], msg.ranges[11], msg.ranges[12], msg.ranges[13], msg.ranges[14], msg.ranges[15], msg.ranges[16], msg.ranges[17], msg.ranges[18], msg.ranges[19], msg.ranges[20], msg.ranges[21], msg.ranges[22], msg.ranges[23], msg.ranges[24], msg.ranges[25], msg.ranges[26], msg.ranges[27], msg.ranges[28], msg.ranges[29], msg.ranges[30], msg.ranges[31], msg.ranges[32], msg.ranges[33], msg.ranges[34], msg.ranges[35], msg.ranges[36], msg.ranges[37], msg.ranges[38], msg.ranges[39], msg.ranges[40], msg.ranges[41], msg.ranges[42], msg.ranges[43], msg.ranges[44], msg.ranges[45]]
        self.minimalVorneLinks = min(vorneLinks)

        vorneRechts = [msg.ranges[305], msg.ranges[306], msg.ranges[307], msg.ranges[308], msg.ranges[309], msg.ranges[310], msg.ranges[311], msg.ranges[312], msg.ranges[313], msg.ranges[314], msg.ranges[315], msg.ranges[316], msg.ranges[317], msg.ranges[318], msg.ranges[319], msg.ranges[320], msg.ranges[321], msg.ranges[322], msg.ranges[323], msg.ranges[324], msg.ranges[325], msg.ranges[326], msg.ranges[327], msg.ranges[328], msg.ranges[329], msg.ranges[330], msg.ranges[331], msg.ranges[332], msg.ranges[333], msg.ranges[334], msg.ranges[335], msg.ranges[336], msg.ranges[337], msg.ranges[338], msg.ranges[339], msg.ranges[340], msg.ranges[341], msg.ranges[342], msg.ranges[343]]
        self.minimalVorneRechts = min(vorneRechts)

        hinten = [msg.ranges[173], msg.ranges[174], msg.ranges[175], msg.ranges[176], msg.ranges[177], msg.ranges[178], msg.ranges[179], msg.ranges[180], msg.ranges[181], msg.ranges[182], msg.ranges[183], msg.ranges[184], msg.ranges[185], msg.ranges[186], msg.ranges[187]]
        self.minimalHinten = min(hinten)

        links = [msg.ranges[90], msg.ranges[91], msg.ranges[92], msg.ranges[93], msg.ranges[94], msg.ranges[95], msg.ranges[96], msg.ranges[97], msg.ranges[89], msg.ranges[88], msg.ranges[87], msg.ranges[86], msg.ranges[85], msg.ranges[84], msg.ranges[83]]
        self.minimalLinks = min(links)

        rechts = [msg.ranges[270], msg.ranges[271], msg.ranges[272], msg.ranges[273], msg.ranges[274], msg.ranges[275], msg.ranges[276], msg.ranges[277], msg.ranges[269], msg.ranges[268], msg.ranges[267], msg.ranges[266], msg.ranges[265], msg.ranges[264], msg.ranges[263]]
        self.minimalRechts = min(rechts)

    def getVorne(self):
        self.spinner()
        return self.minimalVorne

    def getVorneRechts(self):
        self.spinner()
        return self.minimalVorneRechts

    def getVorneLinks(self):
        self.spinner()
        return self.minimalVorneLinks

    def getRechts(self):
        self.spinner()
        return self.minimalRechts

    def getLinks(self):
        self.spinner()
        return self.minimalLinks

    def getHinten(self):
        self.spinner()
        return self.minimalHinten

    def driveStopp(self):
        i = 1
        rospy.init_node('Ride')
        publisher = rospy.Publisher('/Ride', Int64, queue_size=1)
        rate = rospy.Rate(10)
        time.sleep(.1)
        while not rospy.is_shutdown():
            publisher.publish(0)
            rate.sleep()
            if i > 1:
                break
            i = i + 1

    def driveForward(self):
        i = 1
        rospy.init_node('Ride')
        publisher = rospy.Publisher('/Ride', Int64, queue_size=1)
        rate = rospy.Rate(10)
        time.sleep(.1)
        while not rospy.is_shutdown():
            publisher.publish(1)
            rate.sleep()
            if i > 1:
                break
            i = i + 1

    def driveBackward(self):
        i = 1
        rospy.init_node('Ride')
        publisher = rospy.Publisher('/Ride', Int64, queue_size=1)
        rate = rospy.Rate(10)
        time.sleep(.1)
        while not rospy.is_shutdown():
            publisher.publish(2)
            rate.sleep()
            if i > 1:
                break
            i = i + 1

    def steerMiddle(self):
        i = 1
        rospy.init_node('Ride')
        publisher = rospy.Publisher('/servoAngle', Int64, queue_size=1)
        rate = rospy.Rate(10)
        time.sleep(.1)
        while not rospy.is_shutdown():
            publisher.publish(1)
            rate.sleep()
            if i > 1:
                break
            i = i + 1

    def steerRight(self):
        i = 1
        rospy.init_node('Ride')
        publisher = rospy.Publisher('/servoAngle', Int64, queue_size=1)
        rate = rospy.Rate(10)
        time.sleep(.1)
        while not rospy.is_shutdown():
            publisher.publish(2)
            rate.sleep()
            if i > 1:
                break
            i = i + 1

    def steerLeft(self):
        i = 1
        rospy.init_node('Ride')
        publisher = rospy.Publisher('/servoAngle', Int64, queue_size=1)
        rate = rospy.Rate(10)
        time.sleep(.1)
        while not rospy.is_shutdown():
            publisher.publish(0)
            rate.sleep()
            if i > 1:
                break
            i = i + 1

    def evalEvents(self):

        self.prevState = self.state
        self.state = self.nextState

        if self.state == STATES.CHECK_FRONT:
            if self.getVorne() >= self.distance_normal_front:
                self.nextState = STATES.DRIVE_FORWARD
            else:
                self.nextState = STATES.CHECK_FRONT_LR

        if self.state == STATES.CHECK_FRONT_LR:
            if (self.getVorneLinks() >= self.distance_normal_front_left) and (self.getVorneRechts() < self.distance_normal_front_right):
                self.nextState = STATES.STEER_LEFT
            elif (self.getVorneRechts() >= self.distance_normal_front_right) and (self.getVorneLinks() < self.distance_normal_front_left):
                self.nextState = STATES.STEER_RIGHT
            elif (self.getVorneRechts() >= self.distance_normal_front_right) and (self.getVorneLinks() >= self.distance_normal_front_left):
                self.nextState = STATES.STEER_RIGHT
            else:
                self.nextState = STATES.CHECK_BACK

        if self.state == STATES.CHECK_BACK:
            if self.getHinten() >= self.distance_normal_back:
                self.nextState = STATES.DRIVE_BACKWARD
            else:
                self.nextState = STATES.CHECK_LR

        if self.state == STATES.CHECK_LR:
            if (self.getLinks() >= self.distance_normal_left) and (self.getRechts() < self.distance_normal_right):
                self.nextState = STATES.STEER_RIGHT
            elif (self.getRechts() >= self.distance_normal_right) and (self.getLinks() < self.distance_normal_left):
                self.nextState = STATES.STEER_LEFT
            elif self.getHinten() >= self.distance_backward_back:
                self.nextState = STATES.DRIVE_BACKWARD

        if self.state == STATES.DRIVE_FORWARD:
            self.nextState = STATES.CHECK_FRONT

        if self.state == STATES.DRIVE_BACKWARD:
            self.nextState = STATES.CHECK_BACK

        if self.state == STATES.STEER_LEFT:
            if self.prevState == STATES.CHECK_FRONT_LR:
                self.nextState = STATES.DRIVE_FORWARD
            elif self.prevState == STATES.CHECK_LR:
                    self.nextState = STATES.DRIVE_BACKWARD

        if self.state == STATES.STEER_RIGHT:
            if self.prevState == STATES.CHECK_FRONT_LR:
                self.nextState = STATES.DRIVE_FORWARD
            elif self.prevState == STATES.CHECK_LR:
                self.nextState = STATES.DRIVE_BACKWARD

        if self.state == STATES.ERROR:
            self.nextState = STATES.CHECK_FRONT

    def evalState(self):
        if self.state == STATES.CHECK_FRONT:
            print("CHECK_FRONT")  # -----> debugging
            self.driveStopp()

        if self.state == STATES.CHECK_FRONT_LR:
            print("CHECK_FRONT_LR")  # -----> debugging
            self.driveStopp()

        if self.state == STATES.CHECK_BACK:
            print("CHECK_BACK")  # -----> debugging
            self.driveStopp()

        if self.state == STATES.CHECK_LR:
            print("CHECK_LR")  # -----> debugging
            self.driveStopp()

        if self.state == STATES.DRIVE_FORWARD:
            print("DRIVE_FORWARD")  # -----> debugging
            self.driveForward()

        if self.state == STATES.DRIVE_BACKWARD:
            print("DRIVE_BACKWARD")  # -----> debugging
            self.driveBackward()

        if self.state == STATES.STEER_LEFT:
            print("STEER_LEFT")  # -----> debugging
            self.steerLeft()

        if self.state == STATES.STEER_RIGHT:
            print("STEER_RIGHT")  # -----> debugging
            self.steerRight()

        if self.state == STATES.ERROR:
            print("ERROR")  # -----> debugging
            self.driveStopp()
            self.steerMiddle()
