#! /usr/bin/env python

import rospy
from std_msgs.msg import Int64
import time

from driver import FSM
from driver import SCANNER

fahren = FSM()
scannen = SCANNER()

while True:
	scannen.spinner()
	time.sleep(0.5)	
	fahren.evalEvents()
	time.sleep(0.5)
	fahren.evalState()
	time.sleep(0.5)