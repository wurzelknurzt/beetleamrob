#! /usr/bin/env python

import rospy
from sensor_msgs.msg import LaserScan

rospy.init_node('scan_values')
sub = rospy.Subscriber('/scan', LaserScan, callback)
rospy.spin()

def callback(msg):
	vorne = [msg.ranges[353], msg.ranges[354], msg.ranges[355], msg.ranges[356], msg.ranges[357], msg.ranges[358], msg.ranges[359], msg.ranges[0], msg.ranges[1], msg.ranges[2], msg.ranges[3], msg.ranges[4], msg.ranges[5], msg.ranges[6], msg.ranges[7]]
	minimalVorne = min(vorne)

	vorneLinks = [msg.ranges[7], msg.ranges[8], msg.ranges[9], msg.ranges[10], msg.ranges[11], msg.ranges[12], msg.ranges[13], msg.ranges[14], msg.ranges[15], msg.ranges[16], msg.ranges[17], msg.ranges[18], msg.ranges[19], msg.ranges[20], msg.ranges[21], msg.ranges[22], msg.ranges[23], msg.ranges[24], msg.ranges[25], msg.ranges[26], msg.ranges[27], msg.ranges[28], msg.ranges[29], msg.ranges[30], msg.ranges[31], msg.ranges[32], msg.ranges[33], msg.ranges[34], msg.ranges[35], msg.ranges[36], msg.ranges[37], msg.ranges[38], msg.ranges[39], msg.ranges[40], msg.ranges[41], msg.ranges[42], msg.ranges[43], msg.ranges[44], msg.ranges[45]]
	minimalVorneLinks = min(vorneLinks)

	vorneRechts = [msg.ranges[305], msg.ranges[306], msg.ranges[307], msg.ranges[308], msg.ranges[309], msg.ranges[310], msg.ranges[311], msg.ranges[312], msg.ranges[313], msg.ranges[314], msg.ranges[315], msg.ranges[316], msg.ranges[317], msg.ranges[318], msg.ranges[319], msg.ranges[320], msg.ranges[321], msg.ranges[322], msg.ranges[323], msg.ranges[324], msg.ranges[325], msg.ranges[326], msg.ranges[327], msg.ranges[328], msg.ranges[329], msg.ranges[330], msg.ranges[331], msg.ranges[332], msg.ranges[333], msg.ranges[334], msg.ranges[335], msg.ranges[336], msg.ranges[337], msg.ranges[338], msg.ranges[339], msg.ranges[340], msg.ranges[341], msg.ranges[342], msg.ranges[343]]
	minimalVorneRechts = min(vorneRechts)

	hinten = [msg.ranges[173], msg.ranges[174], msg.ranges[175], msg.ranges[176], msg.ranges[177], msg.ranges[178], msg.ranges[179], msg.ranges[180], msg.ranges[181], msg.ranges[182], msg.ranges[183], msg.ranges[184],msg.ranges[185], msg.ranges[186], msg.ranges[187]]
	minimalHinten = min(hinten)

	links = [msg.ranges[90], msg.ranges[91], msg.ranges[92], msg.ranges[93], msg.ranges[94], msg.ranges[95], msg.ranges[96], msg.ranges[97], msg.ranges[89], msg.ranges[88], msg.ranges[87], msg.ranges[86], msg.ranges[85], msg.ranges[84], msg.ranges[83]]
	minimalLinks = min(links)

	rechts = [msg.ranges[270], msg.ranges[271], msg.ranges[272], msg.ranges[273], msg.ranges[274], msg.ranges[275], msg.ranges[276], msg.ranges[277], msg.ranges[269], msg.ranges[268], msg.ranges[267], msg.ranges[266], msg.ranges[265], msg.ranges[264], msg.ranges[263]]
	minimalRechts = min(rechts)

def getVorne():
	return minimalVorne

def getVorneRechts():
	return minimalVorneRechts

def getVorneLinks():
	return minimalVorneLinks

def getRechts():
	return minimalRechts

def getLinks():
	return minimalLinks

def getHinten():
	return minimalHinten

