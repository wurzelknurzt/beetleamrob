#define USE_USBCON
#include <ros.h>
#include <std_msgs/String.h>

ros::NodeHandle nh;
std_msgs::String str_msg;
ros::Publisher chatter("chatter", &str_msg);

char hello[13] = "hello world!";
long distance;

void setup() {
  nh.initNode();
  nh.advertise(chatter);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void loop() {
  distance = getEntfernung();
  if (distance!=0) {
  sprintf(hello, "%d", distance);
  str_msg.data = hello;
  chatter.publish( &str_msg );
  nh.spinOnce();
  delay(50);
}



























/* ********** ********** ********** ********** ********** ********** ********** ********** ********** ********** */
//Author: Steffen
//Letze Änderung: 21.11.2018
//
//Programm zum Ansteuern des Motorcontrollers und der Lenkservos, sowie dem Auslesen der Ultraschallsensoren
//
//Was muss in einem Node gepublished werden, damit es für ROS zur Verfügung steht? 
//Wenn eine Entfernung nicht mehr ausreichend ist (vorne und hinten) muss dies an ROS weitergegeben werden!
//
//Was muss für das Programm zur Verfügung stehen, damit das Auto fahren kann?
//Für das Fahren und das Lenken müssen ein Fahrmodus und ein Lenkwinkel gegeben sein. 

/* ********** ********** ********** ********** ********** ********** ********** ********** ********** ********** */
//Bibliotheken
#include <Servo.h> 

/* ********** ********** ********** ********** ********** ********** ********** ********** ********** ********** */
//Arbeitsvariablen 
int input = 0; //Eingabe des Fahrmodus
int inputAngle = 0; //Eingabe des Lenkwinkels

/* ********** ********** ********** ********** ********** ********** ********** ********** ********** ********** */
//Wartezeiten für die Rückwärtsfahrt
#define bTimeA 20
#define bTimeB 50
#define bTimeC 500

/* ********** ********** ********** ********** ********** ********** ********** ********** ********** ********** */
//Sicherheitsabstand [cm] für die Ultraschallsensoren
#define safetyDistance 50

/* ********** ********** ********** ********** ********** ********** ********** ********** ********** ********** */
//Wartezeiten für die Ultraschallsensoren
#define uTimeA 5
#define uTimeB 10

/* ********** ********** ********** ********** ********** ********** ********** ********** ********** ********** */
//Pinbelegung -> VCC = 5V / GND
#define pinUSTriggerFRONT 4
#define pinUSEchoFRONT 5
#define pinMotor 6
#define pinUSTriggerBACK 7
#define pinUSEchoBACK 8
#define pinSteering 9

/* ********** ********** ********** ********** ********** ********** ********** ********** ********** ********** */
//Motor
Servo servoMotor;
#define stopp 90
#define forward 100 
#define backward 78 

/* ********** ********** ********** ********** ********** ********** ********** ********** ********** ********** */
//Lenkservo
Servo servoSteering;
#define neutral 1425
#define rightMax 1725
#define leftMax 1125

/* ********** ********** ********** ********** ********** ********** ********** ********** ********** ********** */
//ENUM für die FSM zur Motoransteuerung
typedef enum {STOPP,RAMP_FORWARD,FORWARD,BREAK_FORWARD,RAMP_BACKWARD,BACKWARD,BREAK_BACKWARD,SAFETY_STOPP} state_t;
state_t state = STOPP; //Setzen des Startzustandes
state_t nextState = STOPP;

/* ********** ********** ********** ********** ********** ********** ********** ********** ********** ********** */
void setup() {
  //"Debugging"
  Serial.begin(9600); 

  //Ultraschallsensor (vorne)
  pinMode(pinUSTriggerFRONT, OUTPUT);
  pinMode(pinUSEchoFRONT, INPUT);

  //Ultraschallsensor (hinten)
  pinMode(pinUSTriggerBACK, OUTPUT);
  pinMode(pinUSEchoBACK, INPUT);

  //Motor
  servoMotor.attach(pinMotor);

  //Lenkservo
  servoSteering.attach(pinSteering);
  servoSteering.writeMicroseconds(neutral);
}

int i = 0;
/* ********** ********** ********** ********** ********** ********** ********** ********** ********** ********** */
void loop() {
  
  /* ********** ********** ********** temporary ********** ********** ********** */
  if (analogRead(A3) <= 400) {
    input = -1;
  } else if (analogRead(A3) >= 600) {
    input = 1;
  } else {
    input = 0;
  }
  if (analogRead(A4) <= 400) {
    inputAngle = 60;
  } else if (analogRead(A4) >= 600) {
    inputAngle = -60;
  } else {
    inputAngle = 0;
  }
  /* ********** ********** ********** temporary ********** ********** ********** */

  //stateMachineDrive(input, isDistanceOK(ultrasonic(pinUSTriggerFRONT, pinUSEchoFRONT)), isDistanceOK(ultrasonic(pinUSTriggerBACK, pinUSEchoBACK))); //Aufruf des Zustandsautomaten
  stateMachineDrive(input, false, false);
  
  steering(inputAngle); //Ansteuerung der Lenkung 
  i += 1;
  Serial.println(i);
}

/* ********** ********** ********** ********** ********** ********** ********** ********** ********** ********** */
void stateMachineDrive(int inputMode, bool front, bool back) {
    
  state_t state = nextState; 
  //STOPP,RAMP_FORWARD,FORWARD,FAST_FORWARD,BREAK_FORWARD,RAMP_BACKWARD,BACKWARD,BREAK_BACKWARD,SAFETY_STOPP
  
  switch(state) {
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~STOPP~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    case STOPP:
      servoMotor.write(stopp); //Motor ist aus
      
      if (front && (inputMode == 1)) {
        nextState = SAFETY_STOPP;
      } else if (back && (inputMode == -1)) {
        nextState = SAFETY_STOPP;
      } else if (inputMode == 1) { //Anfahren vorwärts
        nextState = RAMP_FORWARD; 
      } else if (inputMode == -1) { //Anfahren rückwärts
        nextState = RAMP_BACKWARD;
      } else { //Kein Zustandswechsel
        nextState = STOPP;
      }
      break;
      
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~RAMP_FORWARDP~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    case RAMP_FORWARD: //Anfahren mit sofortigem Übergang in die Vorwärtsfahrt
      for (int i = stopp; i < forward; i++) {
        int motor = forward;
        motor += 1;
        servoMotor.write(motor);
        delay(1);
      }
      
      nextState = FORWARD; 
      break;
      
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~FORWARD~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    case FORWARD:
      servoMotor.write(forward); //Motor fährt vorwärts
      
      if (inputMode == 0) { //Bremsen
        nextState = BREAK_FORWARD;
      } else if (front) { //Nothalt
        nextState = SAFETY_STOPP; 
      } else { //Kein Zustandswechsel
        nextState = FORWARD;
      }
      break;
      
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~BREAK_FORWARD~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    case BREAK_FORWARD:
      for (int i = forward; i > stopp; i--) {
        int motor = forward;
        motor -= 1;
        servoMotor.write(motor);
        delay(1);
      }

      nextState = STOPP;
      break;
      
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~RAMP_BACKWARD~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    case RAMP_BACKWARD:
      servoMotor.write(stopp); //Umschalten auf Rückwärts
      delay(bTimeA);
      servoMotor.write(backward);
      delay(bTimeB);
      servoMotor.write(stopp);
      delay(bTimeC);
      servoMotor.write(backward);
      for (int i = stopp-1; i > backward; i--) {
        int motor = backward;
        motor -= 1;
        servoMotor.write(motor);
        delay(1);
      }

      nextState = BACKWARD;
      break;
      
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~BACKWARD~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    case BACKWARD:
      servoMotor.write(backward); //Motor fährt rückwärts

      if (inputMode == 0) { //Bremsen
        nextState = BREAK_BACKWARD; 
      } else if (back) { //Nothalt
        nextState = SAFETY_STOPP;
      } else {
        nextState = BACKWARD;
      }
      break;
      
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~BREAK_BACKWARD~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    case BREAK_BACKWARD:
      for (int i = backward; i < stopp; i++) {
        int motor = backward;
        motor += 1;
        servoMotor.write(motor);
        delay(1);
      }

      nextState = STOPP;
      break;
      
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~SAFETY_STOPP~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    case SAFETY_STOPP:
      servoMotor.write(stopp); //Motor ist aus   
          
      nextState = STOPP;
      break;
  }
}

/* ********** ********** ********** ********** ********** ********** ********** ********** ********** ********** */
void steering (int steeringAngle) { //Input: Lenkwinkel im Bereich +-60°
  if ((steeringAngle >= -60)&&(steeringAngle <= 60)) {  //Gültiger Lenkwinkel zwischen +-60°
    if (steeringAngle > 0) {  //Lenken nach RECHTS (Winkelbereich 1° bis 60°)
      servoSteering.writeMicroseconds(neutral + map(steeringAngle, leftMax, rightMax, -60, 60));
    } else if (steeringAngle < 0) {  //Lenken nach LINKS (Winkelbereich -60° bis -1°)
      servoSteering.writeMicroseconds(neutral - map(steeringAngle, leftMax, rightMax, -60, 60));
    } else {  //Geradeaus Lenken, da kein Lenkwinkel
      servoSteering.writeMicroseconds(neutral);
    }
  } else {  //Ungültiger Lenkwinkel
    servoSteering.writeMicroseconds(neutral);
  }
}

/* ********** ********** ********** ********** ********** ********** ********** ********** ********** ********** */
long ultrasonic(int pinTrigger, int pinEcho) {
  long ultrasonicDuration = 0.0; 
  long ultrasonicDistance = 0.0;
  digitalWrite(pinTrigger, LOW); 
  delay(uTimeA);
  digitalWrite(pinTrigger, HIGH); 
  delay(uTimeB);
  digitalWrite(pinTrigger, LOW);
  ultrasonicDuration = pulseIn(pinEcho, HIGH); 
  ultrasonicDistance = (ultrasonicDuration/2.0) * 0.03432;
  return ultrasonicDistance;
}

/* ********** ********** ********** ********** ********** ********** ********** ********** ********** ********** */
bool isDistanceOK(long distance) {
  if ((distance > 0) && (distance < 500)) {
    if (distance < safetyDistance) {
      return true;
    }
  }
  return false;
}






